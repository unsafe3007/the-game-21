import random
import time

cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] * 4
count = 0
dealer_count = 0
AMOUNT = 21
DEALER_AMOUNT_1 = 16
DEALER_AMOUNT_2 = 19

def menu():
    start = input('1 - Game\n2 - Rules\n3 - Exit\n> ').lower()
    if start == '1':
        start_game(get_card, get_dealer_card)
    elif start == '2':
        get_rules()


def is_game_continue():
    return input('Play again? (y/n): ')


def clear_result():
    global count
    global dealer_count

    count = 0
    dealer_count = 0


def get_card():
    global count

    random.shuffle(cards)

    current = cards.pop()

    count += current

    return print(f'Card is {current}'), print(f'Player score: -{count}-\n')


def get_dealer_card():
    global dealer_count

    random.shuffle(cards)

    dealer_current = cards.pop()

    dealer_count += dealer_current

    return print(f'Dealer\'s card is: {dealer_current}'), print(f'Dealer score: -{dealer_count}-\n')


def get_rules():
    while True:
        print('''
        --------------------ПРАВИЛА ИГРЫ--------------------

        Каждый ход игрок может взять карту либо остановиться.
        Если игрок берет карту он получает от 1 до 10 очков.
        Если сумма очков игрока больше 21 - он проиграл.
        Если игрок остановился, ход переходит к дилеру.
        ''')
        back = input('Back to menu? (y/n): ')
        if back == 'y':
            return menu()
        else:
            break


def first_game_end_conditions(count, dealer_count):
    if count >= AMOUNT or dealer_count >= AMOUNT:
        if count > AMOUNT and (dealer_count >= AMOUNT or dealer_count <= AMOUNT):
            print('You lose!')

            return True

        elif count <= AMOUNT and dealer_count > AMOUNT:
            print('You win!')

            return True


def second_game_end_conditions(count, dealer_count):
    condition_for_lose = (DEALER_AMOUNT_1 < dealer_count <= AMOUNT and AMOUNT > count < dealer_count) or (count > AMOUNT and dealer_count <= AMOUNT)
    condition_for_win = (dealer_count > AMOUNT and count <= AMOUNT) or (dealer_count < AMOUNT and count == AMOUNT) or (
            dealer_count < count < AMOUNT)

    if condition_for_lose:
        print('You lose!')

        return True

    elif condition_for_win:
        print('You win!')

        return True

    elif count == dealer_count or (count > AMOUNT and dealer_count > AMOUNT):
        print('Draw!')

        return True


def start_game(get_card, get_dealer_card):
    while True:

        get_card()

        time.sleep(2)

        if dealer_count < DEALER_AMOUNT_2:
            get_dealer_card()

        time.sleep(1)

        if first_game_end_conditions(count, dealer_count):
            break

        choose = input(f"More?\nPress 'Enter' to continue or 'no' to stop: \n").lower()

        if choose == 'no':
            while dealer_count <= DEALER_AMOUNT_1:
                time.sleep(1)

                get_dealer_card()

            if second_game_end_conditions(count, dealer_count):
                break


menu()

again = is_game_continue()

while again == 'y':
    clear_result()
    start_game(get_card, get_dealer_card)

    again = is_game_continue()
else:
    clear_result()
    menu()


